'use strict';

var server = require('server');
var URLUtils = require('dw/web/URLUtils');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var Transaction = require('dw/system/Transaction');
var Request = require('dw/system/Request');

server.get(
    'Create',
    server.middleware.https,
    function (req, res, next) {
        var currentTimeStamp = StringUtils.formatCalendar(new Calendar(new Date()), "yyyyMMddHHmmssSS");
        var customPrefObject = dw.system.Site.getCurrent().getCustomPreferenceValue("newsletter");

        var name = req.querystring.fname;
        var lastname = req.querystring.lname;
        var email = req.querystring.email;

        if (name === undefined || lastname === undefined || email === undefined) {
            var jvariab = {};

            jvariab.success = false;

            res.json(jvariab);
        }
        else {

            Transaction.wrap(function () {
                var customObject = CustomObjectMgr.createCustomObject(customPrefObject, currentTimeStamp);

                customObject.custom.fname = name;
                customObject.custom.lname = lastname;
                customObject.custom.email = email;

                var jvariab = {};

                jvariab.success = true;
                jvariab.idcust = customObject.custom.timestamp;

                res.json(jvariab);
            });
        }
        next();
    });

server.get(
    'Show',
    server.middleware.https,
    function (req, res, next) {

        var customPrefObject = dw.system.Site.getCurrent().getCustomPreferenceValue("newsletter");
        var customObjectListIter = CustomObjectMgr.getAllCustomObjects(customPrefObject);
        var newslist = [];

        while (customObjectListIter.hasNext()) {
            var customObjectList = customObjectListIter.next();
            var item = {};
            item.id = customObjectList.custom.timestamp;
            item.fname = customObjectList.custom.fname;
            item.lname = customObjectList.custom.lname;
            item.email = customObjectList.custom.email;

            newslist.push(item);
        }

        customObjectListIter.close();
        res.json(newslist);

        next();

    });

server.get(
    'Delete',
    server.middleware.https,
    function (req, res, next) {

        var customPrefObject = dw.system.Site.getCurrent().getCustomPreferenceValue("newsletter");
        var id = req.querystring.id;
        var CustomObjDel = CustomObjectMgr.getCustomObject(customPrefObject, id);

        try {
            Transaction.wrap(function () {
                CustomObjectMgr.remove(CustomObjDel);
            });

            var renderiz = {
                success: true
            };

            res.json(renderiz);
        }
        catch (error) {
            var renderiz = {
                success: false
            };

            res.json(renderiz);
        }
        next();

    });

module.exports = server.exports();