'use strict';

var server = require('server');
var PaymentMgr = require('dw/order/PaymentMgr');

server.get(
    'Show',
    server.middleware.https,
    function (req, res, next) {

        var paymentList = PaymentMgr.getActivePaymentMethods();
        var paymentListIter = paymentList.iterator();
        var paylist = [];

        while (paymentListIter.hasNext()) {
            var customPayList = paymentListIter.next();
            var item = {};
            item.id = customPayList.custom.id;
            item.countries = customPayList.custom.countries;
            item.currencies = customPayList.custom.currencies;

            paylist.push(item);
        }

        paymentListIter.close();
        res.json(paylist);

        next();

    });

    module.exports = server.exports();