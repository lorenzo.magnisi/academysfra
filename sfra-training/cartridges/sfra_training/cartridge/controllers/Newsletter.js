'use strict';

var server = require('server');
var URLUtils = require('dw/web/URLUtils');
//Use the following for CSRF protection: add middleware in routes and hidden field on form
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');


server.get(
    'Show',
    server.middleware.https, csrfProtection.generateToken,
    function (req, res, next) {
        var actionUrl = dw.web.URLUtils.url('Newsletter-Subscribe');
        var newsletterForm = server.forms.getForm('newsletter');
        newsletterForm.clear();

        res.render('/newsletter/newslettersignup', {
            actionUrl: actionUrl,
            newsletterForm: newsletterForm
        });

        next();
    }
);

server.post(
    'Subscribe',
    server.middleware.https,
    function (req, res, next) {
        var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
        var newsletform = server.forms.getForm('newsletter');
        var firstname = newsletform.fname.htmlValue;
        var lastname = newsletform.lname.htmlValue;
        var email = newsletform.email.htmlValue;

        var service = newsletterHelpers.getService('crm.newsletter.subscribe');
        var reqObject = {
            properties: {
                firstname: firstname,
                lastname: lastname,
                email: email
            }
        };
        var response = service.call(reqObject);
        // var response = {
        //     ok: false,
        //     msg: "server unavailable"
        // };


        var jsonResponse = {};

        if (response.ok) {
            jsonResponse = {
                firstname: firstname,
                lastname: lastname,
                email: email,
                id: JSON.parse(response.object).id,
                response: response.msg,
                success: true,
                redirectUrl: dw.web.URLUtils.url('Newsletter-Success').toString()
            };
        }
        else {
            var newsAdd = dw.system.HookMgr.callHook('newsletter.error', 'errorHook', reqObject.properties);
            jsonResponse = {
                response: 'There was an error: ' + response.msg,
                success: false
            };;
        }

        res.json(jsonResponse);
        next();
    });

server.post(
    'Handler',
    server.middleware.https, csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var newsletterForm = server.forms.getForm('newsletter');
        var continueUrl = dw.web.URLUtils.url('Newsletter-Show');

        if (newsletterForm.valid) {
            // Show the success page
            res.json({
                success: true,
                redirectUrl: URLUtils.url('Newsletter-Success').toString()
            });
        } else {
            // Handle server-side validation errors here: this is just an example
            res.setStatusCode(500);
            res.json({
                error: true,
                redirectUrl: URLUtils.url('Error-Start').toString()
            });
        }

        next();
    }

);

server.get(
    'List',
    server.middleware.https, csrfProtection.generateToken,
    function (req, res, next) {
        var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
        var limCus = req.querystring.showres;
        var delMsg = req.querystring.msg;
        var service = newsletterHelpers.getService('crm.newsletter.list', limCus);
        var listCust = service.call();
        var listCustParse = JSON.parse(listCust.object);
        var listObj = [];
        
        listCustParse.results.forEach(function takeProp(item) {
            var prop = {
                id : item.id,
                firstname: item.properties.firstname,
                lastname: item.properties.lastname,
                email: item.properties.email
            };
            listObj.push(prop);
        });
        //res.render('/newsletter/newslettercustomlist');
        res.render('newsletter/newslettercustomlist', {
            listObj : listObj,
            delMsg : delMsg
        });

        next();
    }
);

server.post(
    'Delete',
    server.middleware.https, csrfProtection.generateToken,
    function (req, res, next) {
        var newsletterDeleteHelpers = require('*/cartridge/scripts/helpers/newsletterDeleteHelpers');
        var delId = req.form.objId;
        var service = newsletterDeleteHelpers.getService('crm.newsletter.delete', delId);
        var delCust = service.call();
        // var listCustParse = JSON.parse(listCust.object);
        // var listObj = [];
        // listCustParse.results.forEach(function takeProp(item) {
        //     var prop = {
        //         firstname: item.properties.firstname,
        //         lastname: item.properties.lastname,
        //         email: item.properties.email
        //     };
        //     listObj.push(prop);
        // });
        // //res.render('/newsletter/newslettercustomlist');
        // res.render('newsletter/newslettercustomlist', {
        //     listObj : listObj
        // });

        var response = {
            Success : delCust.ok,
            Message : delCust.msg,
            delId : delId
        };
        if (delCust.ok) {
            res.json(response);
             //res.redirect(URLUtils.https('Newsletter-List', 'msg', 'Customer successfully deleted'));
        }
        else {
            //res.redirect(URLUtils.https('Newsletter-List', 'msg', delCust.msg));
        }
        
        next();
    }
);

server.get(
    'Edit',
    server.middleware.https, csrfProtection.generateToken,
    function (req, res, next) {
        
        var actionUrl = dw.web.URLUtils.url('Newsletter-Update', "cusId", req.querystring.cusID);
        var newsletterForm = server.forms.getForm('newsletter');
        newsletterForm.clear();
        var cusId = req.querystring.cusID;

        res.render('/newsletter/newsletteredit', {
            actionUrl: actionUrl,
            newsletterForm: newsletterForm,
            cusId : cusId
        });

        next();
    }
);

server.post(
    'Update',
    server.middleware.https,
    function (req, res, next) {
        var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterUpdateHelpers');
        var newsletform = server.forms.getForm('newsletter');
        var email = newsletform.email.htmlValue;
        var cusId = req.querystring.cusId;

        var service = newsletterHelpers.getService('crm.newsletter.update', cusId);
        var reqObject = {
            properties: {
                email: email
            }
        };
        var response = service.call(reqObject);
        // var response = {
        //     ok: false,
        //     msg: "server unavailable"
        // };


        var jsonResponse = {};

        if (response.ok) {
            jsonResponse = {
                response: 'Update Successfully',
                success: true,
                redirectUrl: dw.web.URLUtils.url('Newsletter-Success').toString()
            };
            res.json(jsonResponse);
        }
        else {
            jsonResponse = {
                response: 'There was an error: ' + response.msg,
                success: false
            };;
        }

        res.json(jsonResponse);
        next();
    });

server.get(
    'Success',
    server.middleware.https,
    function (req, res, next) {
        res.render('/newsletter/newslettersuccess', {
            continueUrl: URLUtils.url('Newsletter-Show'),
            newsletterForm: server.forms.getForm('newsletter')
        });

        next();
    }
);

module.exports = server.exports();