'use strict'

var server = require('server');
var Site = require('dw/system/Site');
var ProductMgr = require('dw/catalog/ProductMgr');
var HookMgr = require('dw/system/HookMgr');

server.get(
    'Show',
    server.middleware.https,
    function (req, res, next) {
        var customProdActive = Site.getCurrent().getCustomPreferenceValue("isAcademyProductActive");

        if (customProdActive === false) {

            res.render("productnotavailable");
        }
        else {

            var productID = Site.getCurrent().getCustomPreferenceValue("productpreference");

            if (productID === null) {

                res.render("productnotavailable");
            }
            else {
                try {
                    var product = ProductMgr.getProduct(productID);
                    var prodarray = {};

                    prodarray = HookMgr.callHook('var.product', 'variables', product);

                    res.render("productavailable", { paramid: prodarray.prodID, prodname: prodarray.name, shdes: prodarray.desc });
                }
                catch (e) {
                    res.render("productnotavailable");
                }
            }
        }
        next();
    });

module.exports = server.exports();