'use strict';

var URLUtils = require('dw/web/URLUtils');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var Transaction = require('dw/system/Transaction');

function errorHook(data) {

    var currentTimeStamp = StringUtils.formatCalendar(new Calendar(new Date()), "yyyyMMddHHmmssSS");
    var customPrefObject = dw.system.Site.getCurrent().getCustomPreferenceValue("newsletter");

    var name = data.firstname;
    var lastname = data.lastname;
    var email = data.email;

    if (name === undefined || lastname === undefined || email === undefined) {
        var jvariab = {};

        jvariab.success = false;

        return jvariab;
    }
    else {

        Transaction.wrap(function () {
            var customObject = CustomObjectMgr.createCustomObject(customPrefObject, currentTimeStamp);

            customObject.custom.fname = name;
            customObject.custom.lname = lastname;
            customObject.custom.email = email;

            var jvariab = {};

            jvariab.success = true;
            jvariab.idcust = customObject.custom.timestamp;

            return jvariab;
        });
    }
}

exports.errorHook = errorHook;