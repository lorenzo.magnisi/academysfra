var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');

function variables(product) {
    var prodID = product.ID;
    var name = product.name;
    var desc = product.shortDescription;
    var currentTimeStamp = StringUtils.formatCalendar(new Calendar(new Date()), "yyyyMMddHHmmssSS");
    
    Transaction.wrap(function () {
        var customProduct = CustomObjectMgr.createCustomObject("AcademyProduct", currentTimeStamp);

        customProduct.custom.ID = prodID;
        customProduct.custom.name = name;
        customProduct.custom.shortDescription = desc;
    });

    return {
        prodID: prodID,
        name: name,
        desc: desc
    };
}

exports.variables = variables;