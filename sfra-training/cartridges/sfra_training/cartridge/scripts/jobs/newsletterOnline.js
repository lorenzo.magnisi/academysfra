var Status = require("dw/system/Status");
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Logger = require('dw/system/Logger');
var Transaction = require('dw/system/Transaction');

function NewsletterOnline() {

    var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
    var customPrefObject = dw.system.Site.getCurrent().getCustomPreferenceValue("newsletter");
    var customObjectListIter = CustomObjectMgr.getAllCustomObjects(customPrefObject);
    var customLog = Logger.getLogger('updateNewsletterOnline', 'error');

    var success = {};

    while (customObjectListIter.hasNext()) {
        var customObjectList = customObjectListIter.next();

        var service = newsletterHelpers.getService('crm.newsletter.subscribe');

        var item = {
            properties: {
                firstname: customObjectList.custom.fname,
                lastname: customObjectList.custom.lname,
                email: customObjectList.custom.email
            }
        };

        var response = service.call(item);

        var jsonResponse = {};

        if (response.ok) {
            Transaction.wrap(function () {
                CustomObjectMgr.remove(customObjectList);
            });
        }
        else {
            if (response.error==409) { //Error: Contact already existing
                jsonResponse = {
                    response: 'Contact already existing ' + customObjectList.custom.timestamp + ' : ' +  customObjectList.custom.email,
                    success: false
                };
                Transaction.wrap(function () {
                    CustomObjectMgr.remove(customObjectList);
                });
                customLog.error(jsonResponse.response);
                success = false;
            } else {
                jsonResponse = {
                    response: 'There was an error for ID {0} :' + response.msg,
                    success: false
                };
                customLog.error(jsonResponse.response, customObjectList.custom.timestamp);
                success = false;
            }
            
        }
    }
    customObjectListIter.close();
    if (success===false) {
        return (new Status(Status.ERROR));
    }
}

exports.NewsletterOnline = NewsletterOnline;