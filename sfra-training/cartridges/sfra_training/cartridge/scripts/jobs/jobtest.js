var Status = require("dw/system/Status");
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');
var Logger = require('dw/system/Logger');

function JobTest() {
    var args = arguments[0];

    idToDel = args.TestParam;

    var customObjectListIter = CustomObjectMgr.getAllCustomObjects("AcademyProduct");
    var i = 0;
    var logList = [];
    var customLog = Logger.getLogger('noIDLog', 'warn');

    while (customObjectListIter.hasNext()) {
        var customObjectList = customObjectListIter.next();
        var item = {};
        item.id = customObjectList.custom.ID;
        //item.fname = customObjectList.custom.name;
        //item.lname = customObjectList.custom.shortDescription;

        logList.push(item);

        if (customObjectList.custom.ID === idToDel) {
            Transaction.wrap(function () {
                CustomObjectMgr.remove(customObjectList);
                i = 1;
            });
        }
    }
    customObjectListIter.close();
    if (i === 1) {
        return (new Status(Status.OK));
    }
    else {
        var stringToPrint = new String("");
        function printerLog(elem, index, array) {
            stringToPrint = stringToPrint + elem.id + " ";
            return(stringToPrint);
        }
        logList.forEach(printerLog);

        customLog.warn('ID not found. Available IDs are {0}', stringToPrint);
        return (new Status(Status.ERROR));
    }

    next();

    return (new Status(Status.OK));
}

exports.JobTest = JobTest;