'use strict';

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

/**
 * Fetches the local service registry assigned to a service id
 * @param {String} serviceId - service id
 * @returns {Object} a local service registry
 */
function getService(serviceId, objId) {

    return LocalServiceRegistry.createService(serviceId, {
        createRequest: function (svc, args) {
            // Default request method is post
            // No need to setRequestMethod
                svc.setRequestMethod("DELETE");
                svc.addHeader("Content-Type", "application/json");
                svc.addHeader("Accept", "application/json");
                var urlKey = dw.system.Site.getCurrent().getCustomPreferenceValue("apikeynewsletter");
                svc.setURL(svc.getURL() + objId);
                svc.addParam("hapikey", urlKey);
                return JSON.stringify(args);
            
        },
        parseResponse: function (svc, client) {
            return client.text;
        },
        mockCall: function (svc, client) {
            return {
                statusCode: 200,
                statusMessage: "Success",
                text: "MOCK RESPONSE (" + svc.URL + ")"
            };
        }
    });
}

module.exports = {
    getService: getService
};