'use strict';

var formValidation = require('base/components/formValidation');

module.exports = {
    submitNewsletter: function () {
        $('form.newsletter-form').on('submit', function (e) {
            var $form = $(this);
            e.preventDefault();
            var url = $form.attr('data-url');
            $form.spinner().start();
            $('form.newsletter-form').trigger('newsletter:submit', e);
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: $form.serialize(),
                success: function (data) {
                    $form.spinner().stop();
                    if (!data.success) {
                        $('.subscription-feedback').text(data.response);
                        $('.subscription-feedback').addClass('failure');
                        $('.newsletter-result').removeClass('hidden');
                    }
                    else {
                        $('.subscription-feedback').text(data.response);
                        $('.subscription-feedback').addClass('success');
                        $('.newsletter-result').removeClass('hidden');
                    }
                },
                error: function (err) {
                    if (err.responseJSON.redirectUrl) {
                        window.location.href = err.responseJSON.redirectUrl;
                    }
                    $form.spinner().stop();
                }
            });
            return false;
        });
    }
};