'use strict';

var formValidation = require('base/components/formValidation');

module.exports = {
    listNewsletter: function () {
        $('form.limitform').on('submit', function (e) {
            var $form = $(this);
            e.preventDefault();
            var url = $form.attr('action');
            $form.spinner().start();
            $.ajax({
                //url: url,
                method: "GET",
                data: $form.serialize(),
                success: function (data) {
                    if (!data) {
                        console.log('ERROR');
                    }
                    else {
                        console.log('SUCCESS');    
                    }
                    $form.spinner().stop();
                },
                error: function (err) {
                    $form.spinner().stop();
                },
                url: url
            });
            return false;
        });
    }
};