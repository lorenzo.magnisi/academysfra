'use strict';

var formValidation = require('base/components/formValidation');

module.exports = {
    listNewsletter: function () {
        $('form.delcust').on('submit', function (e) {
            var $form = $(this);
            e.preventDefault();
            var url = $form.attr('action');
            $form.spinner().start();
            $.ajax({
                url: url,
                method: "POST",
                data: $form.serialize(),
                success: function (data) {
                    if (!data.Success) {
                        console.log('ERROR');
                    }
                    else {
                        var objId = "#item_" + data.delId;
                        $("#ajax").html('<p>Customer successfully deleted</p>');
                        $("#ajax").removeClass('hidden');
                        $(objId).addClass('hidden');
                        console.log('SUCCESS');
                    }
                    $form.spinner().stop();
                },
                error: function (err) {
                    $form.spinner().stop();
                }
            });
            return false;
        });
    }
};